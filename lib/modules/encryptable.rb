module Encryptable
  def encryption_key
    if self.class.name == 'Order'
      if id
        key = id.to_s
        @encryption_key = EncryptionCredential.find_by(source_id: key).try(&:encryption_key)
      else
        @encryption_key ||= create_encryption_key
      end
      return @encryption_key
    end
    raise 'encryption_key not defined for this class, by default its for Order only'
  end

  def create_encryption_key
    SecureRandom.hex(16)
  end

  def save_encryption_key
    key = id.to_s
    raise 'Encryption key is defined already' if EncryptionCredential.exists?(source_id: key)
    raise 'Encryption key is empty' unless @encryption_key.to_s.length > 3

    EncryptionCredential.create!(source_id: key, encryption_key: @encryption_key)
  end

  def delete_encryption_key
    raise 'Encryption key does not exist' unless EncryptionCredential.exists?(source_id: id)

    EncryptionCredential.where(source_id: id).first.try(&:destroy)
  end

  def value_when_no_key
    '[deleted]'
  end

  # we need to override attr_encrypted method so rather than throwing an exception
  # it will return a correct value when no key exists
  # you can also consider overriding encrypt in a similar fashion (although for me it makes sense that no key = you cant edit whats inside)
  def decrypt(attribute, encrypted_value)
    encrypted_attributes[attribute.to_sym][:operation] = :decrypting
    encrypted_attributes[attribute.to_sym][:value_present] = self.class.not_empty?(encrypted_value)
    self.class.decrypt(attribute, encrypted_value, evaluated_attr_encrypted_options_for(attribute))
  rescue ArgumentError # must specify a key
    value_when_no_key
  rescue OpenSSL::Cipher::CipherError # if key was altered
    value_when_no_key
  end

end