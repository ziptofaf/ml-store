namespace :credentials_database do
  task migrate: :environment do
    # you will need to run this on Test environment manually!
    # (with RAILS_ENV=test bundle exec rake credentials_database:migrate)
    require Rails.root.join('db/credentials_database_migrate/20190225081028_create_encryption_credentials')
    ActiveRecord::Base.establish_connection CREDENTIALS_DATABASE
    migrator = ActiveRecord::Migrator.new(:up, [CreateEncryptionCredentials])
    migrator.migrate
  end
end