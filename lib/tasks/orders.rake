namespace :orders do
  task send_to_dops: :environment do
    Rabbit::OrdersPublisher.send_new_orders_to_queue
  end
end