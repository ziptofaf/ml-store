def try_killing_sneakers
  manager_path = File.dirname(__FILE__)
  full_path = File.join(manager_path, '/tmp/pids/sneakers.pid')
  pid = File.open(full_path, 'r').read
  `kill #{pid}`
rescue Errno::ENOENT
  p "pidfile does not exist yet"
end

def start_sneakers
  manager_path = File.dirname(__FILE__)
  Dir.chdir(manager_path)
  command = "bundle exec ruby -e \"require \'sneakers/spawner\';Sneakers::Spawner.spawn\""
  `#{command}`
end

command_name = Hash[ ARGV.join(' ').scan(/--?([^=\s]+)(?:=(\S+))?/) ]
if command_name['action'] == 'start'
  start_sneakers
else
  try_killing_sneakers
end
