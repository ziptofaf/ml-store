# frozen_string_literal: true

# Rabbit module is for everything that directly uses RabbitMQ
module Rabbit
  # used to receive order updates (such as package numbers) from DOPS queue
  class OrdersWorker
    include Sneakers::Worker
    from_queue 'shop_orders_from_dops', env: nil
    def work(queue_output)
      @json = JSON.parse(queue_output)
      find_order
      case @json['action']
      when 'cancel'
        cancel_order
      when 'add_package_number'
        add_package_number_to_order
      else
        raise 'UnrecognizedAction'
      end
      ack!
    end

    private

    def add_package_number_to_order
      @order.package_tracking_url = @json['package_tracking_url']
      @order.state = 'sent'
      @order.save!
    end

    def cancel_order
      @order.state = 'cancelled'
      @order.save!
    end

    def find_order
      @order = Order.find(@json['id'])
    end
  end
end