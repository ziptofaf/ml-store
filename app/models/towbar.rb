class Towbar < ApplicationRecord
  include SellableConcern
  belongs_to :plug, optional: true
  belongs_to :product_category
  enum ball_type: [:fixed, :horizontal, :vertical]
  validates_presence_of :name, :description, :ball_type, :product_category, :price, :weight, :available

  has_many :order_items, as: :sellable
  def description_with_plug
    description + "\n" + plug.try(&:description)
  end

  def public_fields_hash
    hash = { name: name, description: description, ball_type: ball_type, weight: weight, available: available,
             price: price, product_category_id: product_category_id, index_id: index_id, image_url: image_url,
             short_description: short_description, id: id, type: 'Towbar', towbar_model_id: towbar_model_id }
    hash[:plug] = plug.public_fields_hash if plug
    hash
  end

  def self.create_from_json(path)
    f = File.open(path).read
    parsed = JSON.parse(f)
    parsed.each do |record|
      product_category_id = record['product_category_id']
      index_id = record['index_id']
      plug_id = record['plug_id']
      towbar_model_id = record['towbar_model_id']
      record['ball_type'] = self.map_dops_to_store_ball_type(record['ball_type'])
      record['name'] = build_name(record)

      towbar = Towbar.find_or_initialize_by(product_category_id: product_category_id, index_id: index_id,
                                            plug_id: plug_id, towbar_model_id: towbar_model_id)
      towbar.update_attributes!(record)
    end
  end

  def self.generate_image_urls
    base_path = "images/haki-pl/PL."
    Towbar.all.each do |towbar|
      if towbar.plug.nil?
        plug_url = "bez.wiazki"
      else
        mappings = { 'S7' => 1, 'S13' => 2, 'S7TT' => 3, 'S13TT' => 4, 'N7' => 5, 'N13' => 6 }.invert
        plug_url = mappings[towbar.plug.id]
      end
      case towbar.ball_type
      when :fixed, 'fixed'
        towbar_url = "StalyP."
      when :horizontal, 'horizontal'
        towbar_url = "HoryzontalnyA."
      when :vertical, 'vertical'
        towbar_url = "WertykalnyV."
      end
      full_url = base_path + towbar_url + plug_url + '.png'
      towbar.update(image_url: full_url)
    end
  end

  def self.build_name(record)
    name = ""
    case record['ball_type']
    when :fixed
      name += "Hak stały "
    when :horizontal
      name += "Hak zdejmowany horyzontalnie "
    when :vertical
      name += "Hak zdejmowany wertykalnie "
    else
      name += "Hak stały "
    end
    if record['plug_id']
      plug = Plug.find(record['plug_id'])
      name += "z wiązką #{plug.socket}-pin"
    end
    name.strip
  end

    def self.map_dops_to_store_ball_type(ball_type)
    case ball_type
    when 's', 'p'
      :fixed
    when 'a'
      :horizontal
    when 'v'
      :vertical
    else
      :fixed
    end
  end

  scope :available, -> { where('available > 0') }
end
