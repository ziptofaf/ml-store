module SellableConcern
  extend ActiveSupport::Concern
  class_methods do
    # json file has to contain an array, this can also be used to update records, not just create new ones
    # has to include all necessary fields needed to build a towbar/plug etc however
    def import_from_json(path)
      json_array = JSON.parse(File.open(path).read)
      json_array.each do |product_hash|
        plug = find_or_initialize_by(index_id: product_hash['index_id'])
        plug.attributes = product_hash
        plug.save!
      end
    end
  end

  # certain fields probably shouldn't be shown to end users when pulled from API
  # use this to whitelist what is going to be shown
  def public_fields_hash
    raise "Function not defined"
  end


end