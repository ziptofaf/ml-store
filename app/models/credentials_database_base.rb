class CredentialsDatabaseBase < ApplicationRecord
  self.abstract_class = true
  establish_connection CREDENTIALS_DATABASE
end