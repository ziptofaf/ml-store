# specific position in an order, it's connected to the specific product like Towbar or Plug
class OrderItem < ApplicationRecord
  belongs_to :sellable, polymorphic: true
  belongs_to :order
  validates_presence_of :order, :sellable
  before_create :fill_purchase_price

  def hash_with_sellable
    sellable_hash = sellable.public_fields_hash
    JSON.parse(to_json).merge('sellable' => sellable_hash)
  end

  def fill_purchase_price
    self.purchase_price = sellable.price * quantity
  end
end
