class Plug < ApplicationRecord
  include SellableConcern
  belongs_to :product_category
  validates_presence_of :name, :socket, :description, :product_category, :price, :available, :weight, :product_category
  validates_inclusion_of :dedicated, in: [true, false]
  validates_inclusion_of :indicator_failure_bulb, in: [true, false]
  validates_inclusion_of :permanent_power, in: [true, false]
  validates_inclusion_of :bypass_relay, in: [true, false]
  validates_inclusion_of :activation, in: [true, false]
  has_many :order_items, as: :sellable


  def public_fields_hash
    { name: name, description: description, socket: socket, dedicated: dedicated,
      indicator_failure_bulb: indicator_failure_bulb, parking_sensors_switch: parking_sensors_switch,
      permanent_power: permanent_power, bypass_relay: bypass_relay, activation: activation, price: price,
      weight: weight, available: available, product_category_id: product_category_id, index_id: index_id,
      image_url: image_url, short_description: short_description, type: 'Plug', plug_model_id: plug_model_id  }
  end

  scope :available, -> { where('available > 0') }
  scope :old_plugs, -> { where(name: %w[S7 S7TT S13 S13TT]) }
end
