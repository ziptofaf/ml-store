# order, a primary class for purchasing things
# important remark - it uses UUID for it's id type! (that way you cant enter someone else's order details)
# so in migrations use add_column :table_name, :order_id, :uuid or t.references :order, type: :uuid
# if building relationships using this model
# http://clearcove.ca/2017/08/postgres-uuid-as-primary-key-in-rails-5-1 - used this as a resource
# allowed states: 'new', 'cancelled', 'sent'
class Order < ApplicationRecord
  include Encryptable
  belongs_to :user, optional: true
  has_many :order_items
  has_one :payment
  accepts_nested_attributes_for :order_items
  accepts_nested_attributes_for :payment

  before_create :create_encryption_key
  after_create :save_encryption_key

  attr_encrypted :email,
                 :billing_name,
                 :billing_surname,
                 :billing_phone,
                 :billing_address,
                 :billing_city,
                 :billing_post_code,
                 :billing_country,
                 :delivery_name,
                 :delivery_surname,
                 :delivery_phone,
                 :delivery_address,
                 :delivery_city,
                 :delivery_post_code,
                 :tax_number,
                 :delivery_country, key: :cached_encryption_key

  validates_presence_of :email, :billing_name, :billing_surname, :billing_phone, :billing_address, :billing_city,
                        :billing_post_code, :billing_country, :delivery_name, :delivery_surname, :delivery_phone,
                        :delivery_address, :delivery_city, :delivery_country

  # i want to avoid N+1 queries when using hash_with_price
  def cached_encryption_key
    @cached_encryption_key ||= encryption_key
  end

  def self.recently_updated
    Order.joins(:payment).where('payments.paid_at > ?', 12.hours.ago).or(Order.joins(:payment).where('orders.updated_at > ?', 12.hours.ago))
  end

  def delivery_option
    'dpd'
  end
  def delivery_option=(_val)
    nil
  end

  # if delivery should be identical to billing address, this function will just copy all billing_ fields
  def fill_delivery_from_billing
    billing_columns = Order.column_names.select { |name| name.include?('billing') }
    billing_columns.each do |column|
      delivery_column_name = column.gsub('billing', 'delivery') + '='
      billing_value = public_send(column)
      public_send(delivery_column_name, billing_value)
    end
  end

  # return combined price for all products in an order
  def price
    order_items.sum(:purchase_price)
  end
  # due to attr_encrypted you have to call these methods explicitely, cant output database columns directly
  def hash_with_price
    hash = { email: email, billing_name: billing_name, billing_surname: billing_surname, billing_phone: billing_phone,
             billing_address: billing_address, billing_city: billing_city, billing_post_code: billing_post_code,
             billing_country: billing_country, delivery_name: delivery_name, delivery_surname: delivery_surname,
             delivery_phone: delivery_phone, delivery_address: delivery_address, delivery_city: delivery_city,
             delivery_post_code: delivery_post_code, delivery_country: delivery_country, id: id, created_at: created_at,
             comment: comment, package_tracking_url: package_tracking_url, tax_number: tax_number,state: state }
    hash.merge!(price: price)
    hash.merge!(delivery_option: 'dpd') # for now it's the only shipping option available but in the future there might be more
  end

  def self.has_personal_information?
    true
  end


end
