class Payment < ApplicationRecord
  belongs_to :order
  validates :payment_system, inclusion: { in: PaymentProcessors::Abstract.descendants_system_strings,
                                          message: '%{value} is not a valid payment system' }
  before_create :preauthorize_if_applicable
  after_create :generate_payment_url
  # fetches a payment processor that should be used for a given payment
  def payment_processor
    PaymentProcessors::Abstract.system_string_to_descendant(payment_system).new(self)
  end
  # cash on delivery for instance is paid "instantly" upon placing an order
  def preauthorize_if_applicable
    self.paid_at = Time.now if payment_processor.preauthorized?
  end

  def to_hash
    { id: id, paid_at: paid_at, payment_system: payment_system, order_id: order_id, url: url }
  end

  private

  def generate_payment_url
    payment_processor.try(&:payment_url)
  end
end
