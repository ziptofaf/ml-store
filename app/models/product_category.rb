# category element, together multiple elements of this class will create a category tree
# which will be used to find products
class ProductCategory < ApplicationRecord

  has_many :children_categories, class_name: 'ProductCategory', foreign_key: 'root_category'
  belongs_to :parent_category, class_name: 'ProductCategory', foreign_key: 'root_category', optional: true

  # downloads primary categories for the store
  def self.roots
    ProductCategory.where(root_category: nil)
  end

  # categories generally come from DOPS, this function imports a category file from there
  def self.import_from_json(path = Rails.root.join('tmp', 'product_categories.json'))
    f = File.open(path).read
    records = JSON.parse(f)
    records.each do |record|
      category = ProductCategory.find_or_initialize_by(id: record['id'])
      category.attributes = record
      category.save!
    end
  end

  # generates a hash of Node, each having id, name and children
  def as_tree
    Node.new(self)
  end

  # simple class used to store basic info on each category with its children, recursively
  class Node
    attr_accessor :children, :name, :id
    def initialize(category)
      @name = category.name
      @id = category.id
      @children = category.children_categories.map { |child| Node.new(child) }
    end
  end
end
