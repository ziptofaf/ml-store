# this class is used to store authentication data that changes often
# (like Tokens/Bearers) for different Payment systems, like PayU or Paypal
class PaymentAuthentication < ApplicationRecord
  validates :payment_system, inclusion: { in: PaymentProcessors::Abstract.descendants_system_strings,
                                          message: '%{value} is not a valid payment system' }
end
