# frozen_string_literal: true

module Rabbit
  # this class is used to publish info on orders
  class OrdersPublisher
    QUEUE = 'shop_orders_to_dops'
    def self.send_new_orders_to_queue
      Order.recently_updated.each do |order|
        items = { order_items: order.order_items.map(&:hash_with_sellable) }
        payment = { payment: order.payment.to_hash }
        payload = order.hash_with_price.merge(items).merge(payment).merge(store_location: 'pl').to_json
        Rabbit::Publisher.publish(payload, QUEUE)
      end
      true
    rescue => e
      Rails.logger.error e
      false
    end
  end
end