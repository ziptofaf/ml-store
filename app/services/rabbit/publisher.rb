module Rabbit
  # used to communicate with RabbitMQ
  class Publisher
    # used to send a message to a given queue
    # non-durable queues are transient and lost upon server restart - good for testing for instance
    def self.publish(payload, queue, durable = true)
      return false unless valid_json?(payload)

      conn = Bunny.new(host: 'dops.tuger.pl', user: 'shop', password: Rails.application.credentials.shop_queue_password)
      conn.start
      ch = conn.create_channel
      # creates a queue if it doesnt exist, delivery_mode 2 means that messages are also saved to drive
      q = ch.queue(queue, durable: durable, delivery_mode: 2)
      ch.default_exchange.publish(payload, routing_key: q.name)
      conn.close # this has a problem of not keeping a queue alive but frankly it's not much of a problem
      true
    end

    # making sure that only valid JSON strings can be sent from this publisher
    # (has a bit of overhead associated with it but its better than sending malformed data by mistake)
    def self.valid_json?(payload)
      JSON.parse(payload)
      true
    rescue JSON::ParserError
      false
    end
  end
end