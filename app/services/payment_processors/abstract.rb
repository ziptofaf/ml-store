module PaymentProcessors
  class Abstract

    class TokenObtainingFailure < RuntimeError
    end

    attr_reader :payment
    # used in Payment model, to determine valid payment methods available in the system
    def self.descendants_system_strings
      descendants.map(&:system_string)
    end

    def self.system_string_to_descendant(system_string)
      descendants.filter { |d| d.system_string == system_string }.first
    end

    def self.system_string
      raise 'undefined payment system string'
    end

    def initialize(payment)
      @payment = payment
    end

    def payment_url
      raise 'undefined payment url'
    end

    def validate(request)
      raise 'abstract class, cant validate a payment'
    end

    # preauthorized processors allow you to instantly ship the product
    # without waiting for something to process the payment
    # eg. cash on delivery/paid in advance
    def preauthorized?
      false
    end

    def valid_payment?(request)
      raise 'abstract class, cant validate a payment'
    end
  end



end