module PaymentProcessors
  class Wired < Abstract
    def self.system_string
      'wired'
    end

    def payment_url
      nil
    end

    # Wired payment is always valid
    def validate(_request)
      true
    end

    def preauthorized?
      false
    end
  end
end