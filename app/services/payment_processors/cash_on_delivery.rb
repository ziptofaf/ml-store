module PaymentProcessors
  class CashOnDelivery < Abstract
    def self.system_string
      'cash_on_delivery'
    end

    def payment_url
      nil
    end

    # cash on delivery is "always valid" so to speak - as in ready to inform DOPS that product should be shipped
    def validate
      payment.update(paid_at: Time.now)
    end

    def preauthorized?
      true
    end
  end
end
