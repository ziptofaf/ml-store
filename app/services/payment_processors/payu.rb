module PaymentProcessors
  class Payu < Abstract
    def self.system_string
      'payu'
    end

    def payment_url
      return payment.url if payment.url

      url = payment_creation_urls[:payu_url]
      begin
      RestClient.post(url, order_hash.to_json, headers_hash)
      rescue RestClient::Found => e
        response = e.response
        payment.url = JSON.parse(response.body)['redirectUri']
        payment.save!
      end
    end

    def validate(request)
      return unless request['status'] == 'COMPLETED'
      return unless login_credentials[:merchant_pos_id].to_s == request['merchantPosId'].to_s

      payment.update(paid_at: Time.now)
    end

    private

    def authenticate
      payment_authentication = PaymentAuthentication.find_or_initialize_by(payment_system: self.class.system_string)
      return payment_authentication.token if valid_payment_auth?(payment_authentication)

      token = obtain_new_token
      raise TokenObtainingFailure, 'could not obtain a token' unless token

      payment_authentication.token = token[:access_token]
      payment_authentication.expires_at = Time.now + token[:expires_in]
      payment_authentication.save!
      payment_authentication.token
    end

    def valid_payment_auth?(payment_auth)
      payment_auth.expires_at && payment_auth.expires_at > (Time.now + 10.minutes)
    end

    def obtain_new_token
      client_id = login_credentials[:client_id]
      client_secret = login_credentials[:client_secret]
      url = payment_creation_urls[:token_url]
      params = "grant_type=client_credentials&client_id=#{client_id}&client_secret=#{client_secret}"
      res = RestClient.post(url, params)
      return nil unless res.code == 200

      json = JSON.parse(res.body)
      { access_token: json['access_token'], expires_in: json['expires_in'] }
    rescue => e # cant specify a good error class due to it being the internet, 10000 things can go wrong
      nil
    end

    def order_hash
      @order = payment.order
      hash = {}
      hash['notifyUrl'] = 'back.tuger.eu' if Rails.env.production?
      hash['customerIp'] = @order.ip_address
      hash['merchantPosId'] = login_credentials[:merchant_pos_id] # ze strony payu->moje sklepy->sklep->punkty platnosci->klucze konfiguracji
      hash['description'] = 'Tuger.eu, haki samochodowe'
      hash['notifyUrl'] = payment_creation_urls[:notify_url] if payment_creation_urls[:notify_url] # can be nil
      hash['currencyCode'] = 'PLN'
      hash['extOrderId'] = @order.id
      hash['totalAmount'] = (@order.price.to_f * 100).to_i
      hash['buyer'] = buyer_hash
      hash['products'] = order_items_array
      hash['settings'] = {
        'invoiceDisabled' => true
      }
      hash
    end

    def buyer_hash
      {
        'email' => @order.email,
        'phone' => @order.billing_phone,
        'firstName' => @order.billing_name,
        'lastName' => @order.billing_surname,
        'language' => 'pl'
      }
    end

    def headers_hash
      { content_type: 'application/json',
        authorization: "Bearer #{authenticate}" }
    end

    def order_items_array
      @order.order_items.map do |item|
        { 'quantity' => item.quantity,
          'unitPrice' => ((item.purchase_price / item.quantity).to_f * 100).to_i,
          'name' => item.sellable.name }
      end
    end

    def payment_creation_urls
      if Rails.env.production?
        { payu_url: 'https://secure.payu.com/api/v2_1/orders', notify_url: 'https://pl.backend.tuger.eu/payment_listeners/payu',
          token_url: 'https://secure.payu.com/pl/standard/user/oauth/authorize' }
      elsif Rails.env.staging?
        { payu_url: 'https://secure.snd.payu.com/api/v2_1/orders',
          notify_url: 'https://test-api.tuger.eu/payment_listeners/payu',
          token_url: 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize' }
      else
        { payu_url: 'https://secure.snd.payu.com/api/v2_1/orders', notify_url: nil,
          token_url: 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize' }
      end
    end

    def login_credentials
      if Rails.env.production?
        { client_id: Rails.application.credentials.payu_prod_client_id,
          client_secret: Rails.application.credentials.payu_prod_client_secret,
          merchant_pos_id: Rails.application.credentials.payu_prod_merchant_id }
      else
        { client_id: Rails.application.credentials.payu_sand_client_id,
          client_secret: Rails.application.credentials.payu_sand_client_secret,
          merchant_pos_id: Rails.application.credentials.payu_sand_merchant_id }
      end
    end

  end
end
