# frozen_string_literal: true

class OrderItemsController < ApplicationController
  skip_before_action :doorkeeper_authorize!, only: [:index]
  before_action :set_order
  def index
    render json: @order.order_items.map(&:hash_with_sellable)
  end

  private

  def set_order
    @order = Order.find_by(id: params[:order_id])
    unless @order
      render status: 404, json: {
        status: 'error',
        message: 'order does not exist'
      }
    end
  end
end
