# frozen_string_literal: true

class OrdersController < ApplicationController
  skip_before_action :doorkeeper_authorize!, only: [:show, :create]
  before_action :set_order, only: [:show]
  before_action :validate_order_items, only: [:create]
  before_action :validate_payment, only: [:create]

  def index
    orders = current_user.orders.includes(:payment).order(created_at: :desc).first(20)
    arr = []
    orders.each do |order|
      arr.push(order.hash_with_price.merge(payment: order.payment.to_hash))
    end
    render json: arr
  end

  def show
    payment = @order.payment
    render json: @order.hash_with_price.merge(payment: payment.to_hash).merge(status: 'ok')
  end

  def create
    order = Order.new(order_params.merge(ip_address: request.remote_ip))
    order.user = current_user
    if order.save
      payment = order.payment
      render json: { status: 'ok', id: order.id, state: order.state, price: order.price,
                     payment: { id: payment.id, url: payment.url } }
    else
      render status: 400, json: {
        status: 'error',
        message: 'Could not save an order',
        errors: order.errors
      }
    end
  end

  private

  def validate_order_items
    item_params = order_params[:order_items_attributes]
    return if item_params and item_params.count >= 1

    render status: 400, json: {
      status: 'error',
      message: 'Order items are incorrect'
    }
  end

  def validate_payment
    return if order_params[:payment_attributes] and order_params[:payment_attributes][:payment_system]

    render status: 400, json: {
      status: 'error',
      message: 'Payment system is undefined'
    }
  end

  def set_order
    @order = Order.find_by(id: params[:id])
    unless @order
      render status: 404, json: {
        status: 'error',
        message: 'Order does not exist'
      }
    end
  end

  def order_params
    params.require(:order).permit(:email, :billing_name, :billing_surname, :billing_phone,
                                  :billing_address, :billing_city, :billing_post_code,
                                  :billing_country, :delivery_name, :delivery_surname,
                                  :delivery_phone, :delivery_address, :delivery_city,
                                  :delivery_post_code, :delivery_country, :comment, :delivery_option,
                                  :tax_number, payment_attributes: [:payment_system],
                                  order_items_attributes: [:quantity, :sellable_type, :sellable_id])
  end
end
