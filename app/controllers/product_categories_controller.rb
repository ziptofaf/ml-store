# frozen_string_literal: true

class ProductCategoriesController < ApplicationController
  before_action :validate_root_exists, only: :index
  skip_before_action :doorkeeper_authorize!
  def index
    if params[:root_category]
      render json: ProductCategory.where(root_category: params[:root_category]).order(name: :asc).select(:id, :name)
    else
      render json: ProductCategory.where(root_category: nil).order(name: :asc).select(:id, :name)
    end
  end

  private

  def validate_root_exists
    return unless params[:root_category]

    unless ProductCategory.exists?(id: params[:root_category])
      render status: 400, json: {
        status: 'error',
        message: 'Invalid category id'
      }
    end
  end
end
