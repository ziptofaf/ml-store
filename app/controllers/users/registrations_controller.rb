# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  skip_before_action :doorkeeper_authorize!, only: :create
  skip_before_action :authenticate_scope!, only: [:update]

  respond_to :json
  respond_to :html, only: []
  respond_to :xml, only: []

  private

  def sign_up_params
    params.require(:user).permit([
                                   :email,
                                   :password,
                                   :password_confirmation,
                                 ])
  end

  def account_update_params
    params.require(:user).permit([
                                   :email,
                                   :password,
                                   :password_confirmation,
                                   :current_password
                                 ])
  end
end
