class ProductsController < ApplicationController

  before_action :set_products, only: :index
  before_action :set_product, only: :show
  skip_before_action :doorkeeper_authorize!

  def index
    render json: @products.map(&:public_fields_hash)
  end

  def show
    render json: @product.public_fields_hash
  end

  private

  def set_products
    category = params[:product_category]
    render status: 400, json: { status: 'error', message: 'Product Category is invalid' } and return unless ProductCategory.exists?(category)
    @product_category = ProductCategory.find(category)
    set_products_in_category
  end

  def set_products_in_category
    case params[:product_type]
    when 'towbar'
      @products = Towbar.where(product_category: @product_category)
    when 'plug'
      @products = Plug.where(product_category: @product_category)
    when 'all'
      towbars = Towbar.where(product_category: @product_category).to_a
      plugs = Plug.where(product_category: @product_category).to_a
      @products = towbars + plugs
    else
      render status: 400, json: { status: 'error', message: 'Product Type is invalid' } and return
    end
  end

  def set_product
    id = params[:id].to_i
    render status: 400, json: { status: 'error', message: 'Product id is not set' } and return unless id.to_i.positive?
    case params[:product_type]
    when 'towbar'
      render status: 400, json: { status: 'error', message: 'Product id is invalid' } and return unless Towbar.exists?(id: id)
      @product = Towbar.find(id)
    when 'plug'
      render status: 400, json: { status: 'error', message: 'Product id is invalid' } and return unless Plug.exists?(id: id)
      @product = Plug.find(id)
    else
      render status: 400, json: { status: 'error', message: 'Product Type is invalid' } and return
    end
  end

end
