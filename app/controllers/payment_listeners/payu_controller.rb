# frozen_string_literal: true

module PaymentListeners
  # Listener for Payu transactions. It is used with Payment and PaymentProcessors/Payu to update payment status
  class PayuController < ApplicationController
    skip_before_action :doorkeeper_authorize!
    def listen
      render json: { status: 'ok' }
      order = Order.find(payu_params['extOrderId'])
      payment = order.payment
      return unless payment.payment_system == 'payu'

      payment.payment_processor.validate(payu_params)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e
    end

    def payu_params
      params.require(:order).permit('orderId', 'extOrderId', 'orderCreateDate', 'notifyUrl', 'merchantPosId',
                                    'customerIp', 'description', 'currencyCode', 'totalAmount', 'status',
                                    'localReceiptDateTime',
                                    'buyer' => %w[email phone firstName lastName language],
                                    'payMethod' => ['type'],
                                    'products' => %w[name unitPrice quantity], 'properties' => [])
    end
  end
end
