# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :doorkeeper_authorize!

  def current_user
    @current_user ||= User.find(doorkeeper_token[:resource_owner_id])
  rescue NoMethodError
    nil
  end

end
