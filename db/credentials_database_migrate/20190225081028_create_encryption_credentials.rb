class CreateEncryptionCredentials < ActiveRecord::Migration[5.2]
  def change
    create_table :encryption_credentials do |t|
      t.string :source_id
      t.text :encryption_key

      t.timestamps
    end
    add_index :encryption_credentials, :source_id
  end
end
