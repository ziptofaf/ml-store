# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_05_200213) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "oauth_access_grants", force: :cascade do |t|
    t.bigint "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["resource_owner_id"], name: "index_oauth_access_grants_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.bigint "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "order_items", force: :cascade do |t|
    t.decimal "purchase_price", precision: 8, scale: 2, null: false
    t.integer "quantity", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sellable_type"
    t.bigint "sellable_id"
    t.uuid "order_id"
    t.index ["sellable_type", "sellable_id"], name: "index_order_items_on_sellable_type_and_sellable_id"
  end

  create_table "orders", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "package_tracking_url"
    t.string "state", default: "new", null: false
    t.bigint "user_id"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_email"
    t.string "encrypted_email_iv"
    t.string "encrypted_billing_name"
    t.string "encrypted_billing_name_iv"
    t.string "encrypted_billing_surname"
    t.string "encrypted_billing_surname_iv"
    t.string "encrypted_billing_phone"
    t.string "encrypted_billing_phone_iv"
    t.string "encrypted_billing_address"
    t.string "encrypted_billing_address_iv"
    t.string "encrypted_billing_city"
    t.string "encrypted_billing_city_iv"
    t.string "encrypted_billing_post_code"
    t.string "encrypted_billing_post_code_iv"
    t.string "encrypted_billing_country"
    t.string "encrypted_billing_country_iv"
    t.string "encrypted_delivery_name"
    t.string "encrypted_delivery_name_iv"
    t.string "encrypted_delivery_surname"
    t.string "encrypted_delivery_surname_iv"
    t.string "encrypted_delivery_phone"
    t.string "encrypted_delivery_phone_iv"
    t.string "encrypted_delivery_address"
    t.string "encrypted_delivery_address_iv"
    t.string "encrypted_delivery_city"
    t.string "encrypted_delivery_city_iv"
    t.string "encrypted_delivery_post_code"
    t.string "encrypted_delivery_post_code_iv"
    t.string "encrypted_delivery_country"
    t.string "encrypted_delivery_country_iv"
    t.string "ip_address"
    t.string "encrypted_tax_number"
    t.string "encrypted_tax_number_iv"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "payment_authentications", force: :cascade do |t|
    t.string "token"
    t.string "payment_system"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "expires_at"
  end

  create_table "payments", force: :cascade do |t|
    t.datetime "paid_at"
    t.string "payment_system", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "order_id"
    t.string "url"
  end

  create_table "plugs", force: :cascade do |t|
    t.string "name", null: false
    t.integer "socket", null: false
    t.text "description", null: false
    t.boolean "dedicated", null: false
    t.boolean "indicator_failure_bulb", null: false
    t.boolean "parking_sensors_switch", null: false
    t.boolean "permanent_power", null: false
    t.boolean "bypass_relay", null: false
    t.boolean "activation", null: false
    t.decimal "price", precision: 8, scale: 2, null: false
    t.integer "weight", null: false
    t.integer "available", null: false
    t.bigint "product_category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "short_description"
    t.integer "index_id", null: false
    t.string "image_url"
    t.integer "plug_model_id"
    t.index ["product_category_id"], name: "index_plugs_on_product_category_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.integer "root_category"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "towbars", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", null: false
    t.bigint "plug_id"
    t.integer "ball_type", null: false
    t.decimal "price", precision: 8, scale: 2, null: false
    t.integer "weight", null: false
    t.integer "available", null: false
    t.bigint "product_category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "short_description"
    t.date "old_plug_max_date"
    t.integer "index_id", null: false
    t.string "image_url"
    t.integer "towbar_model_id"
    t.index ["plug_id"], name: "index_towbars_on_plug_id"
    t.index ["product_category_id"], name: "index_towbars_on_product_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "orders", "users"
  add_foreign_key "plugs", "product_categories"
  add_foreign_key "towbars", "plugs"
  add_foreign_key "towbars", "product_categories"
end
