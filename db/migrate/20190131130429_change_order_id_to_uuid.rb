class ChangeOrderIdToUuid < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')
    add_column :orders, :uuid, :uuid, default: "gen_random_uuid()", null: false
    remove_column :order_items, :order_id, :integer
    remove_column :payments, :order_id, :integer
    change_table :orders do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE orders ADD PRIMARY KEY (id);"
    add_column :payments, :order_id, :uuid
    add_column :order_items, :order_id, :uuid
  end
end
