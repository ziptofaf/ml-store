class TransformOrderIntoItsEncryptedForm < ActiveRecord::Migration[5.2]
  def change
    symbols = [:email,
               :billing_name,
               :billing_surname,
               :billing_phone,
               :billing_address,
               :billing_city,
               :billing_post_code,
               :billing_country,
               :delivery_name,
               :delivery_surname,
               :delivery_phone,
               :delivery_address,
               :delivery_city,
               :delivery_post_code,
               :delivery_country]
    symbols.each do |symbol|
      remove_column :orders, symbol
      add_column :orders, "encrypted_#{symbol}", :string
      add_column :orders, "encrypted_#{symbol}_iv", :string
    end
  end
end
