class AddImageUrlToTowbars < ActiveRecord::Migration[5.2]
  def change
    add_column :towbars, :image_url, :string
    add_column :plugs, :image_url, :string
  end
end
