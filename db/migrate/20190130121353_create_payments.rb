class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.belongs_to :order, foreign_key: true, null: false
      t.datetime :paid_at
      t.string :payment_system, null: false

      t.timestamps
    end
  end
end
