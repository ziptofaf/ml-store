class AddTowbarModelIdToTowbar < ActiveRecord::Migration[5.2]
  def change
    add_column :towbars, :towbar_model_id, :integer
  end
end
