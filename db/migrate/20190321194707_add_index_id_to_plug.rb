class AddIndexIdToPlug < ActiveRecord::Migration[5.2]
  def change
    add_column :plugs, :index_id, :integer, null: false
  end
end
