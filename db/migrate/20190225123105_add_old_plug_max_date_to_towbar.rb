class AddOldPlugMaxDateToTowbar < ActiveRecord::Migration[5.2]
  def change
    add_column :towbars, :old_plug_max_date, :date
  end
end
