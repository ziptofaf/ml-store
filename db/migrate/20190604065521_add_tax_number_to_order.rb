class AddTaxNumberToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :encrypted_tax_number, :string
    add_column :orders, :encrypted_tax_number_iv, :string
  end
end
