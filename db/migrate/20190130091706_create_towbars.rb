class CreateTowbars < ActiveRecord::Migration[5.2]
  def change
    create_table :towbars do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.belongs_to :plug, foreign_key: true
      t.integer :ball_type, null: false
      t.decimal :price,  precision: 8, scale: 2, null: false
      t.integer :weight, null: false
      t.integer :available, null: false
      t.belongs_to :product_category, foreign_key: true, null: false

      t.timestamps
    end
  end
end
