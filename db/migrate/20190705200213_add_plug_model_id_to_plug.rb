class AddPlugModelIdToPlug < ActiveRecord::Migration[5.2]
  def change
    add_column :plugs, :plug_model_id, :integer
  end
end
