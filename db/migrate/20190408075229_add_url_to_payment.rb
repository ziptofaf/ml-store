class AddUrlToPayment < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :url, :string
  end
end
