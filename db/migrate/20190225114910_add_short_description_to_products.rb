class AddShortDescriptionToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :towbars, :short_description, :text
    add_column :plugs, :short_description, :text
  end
end
