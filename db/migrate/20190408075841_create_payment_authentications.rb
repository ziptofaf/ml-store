class CreatePaymentAuthentications < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_authentications do |t|
      t.string :token
      t.string :payment_system

      t.timestamps
    end
  end
end
