class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :email, null: false
      t.string :billing_name, null: false
      t.string :billing_surname, null: false
      t.string :billing_phone, null: false
      t.string :billing_address, null: false
      t.string :billing_city, null: false
      t.string :billing_post_code, null: false
      t.string :billing_country, null: false
      t.string :delivery_name, null: false
      t.string :delivery_surname, null: false
      t.string :delivery_phone, null: false
      t.string :delivery_address, null: false
      t.string :delivery_city, null: false
      t.string :delivery_post_code, null: false
      t.string :delivery_country, null: false
      t.string :package_tracking_url
      t.string :guid, null: false
      t.string :state, null: false, default: 'new'
      t.belongs_to :user, foreign_key: true
      t.text :comment

      t.timestamps
    end
    add_index :orders, :guid
  end
end
