class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.decimal :purchase_price, precision: 8, scale: 2, null: false
      t.integer :quantity, null: false, default: 1
      t.belongs_to :order, foreign_key: true, null: false
      t.timestamps
      t.references :sellable, polymorphic: true, index: true
    end
  end
end
