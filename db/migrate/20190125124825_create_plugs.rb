class CreatePlugs < ActiveRecord::Migration[5.2]
  def change
    create_table :plugs do |t|
      t.string :name, null: false
      t.integer :socket, null: false
      t.text :description, null: false
      t.boolean :dedicated, null: false
      t.boolean :indicator_failure_bulb, null: false
      t.boolean :parking_sensors_switch, null: false
      t.boolean :permanent_power, null: false
      t.boolean :bypass_relay, null: false
      t.boolean :activation, null: false
      t.decimal :price, precision: 8, scale: 2, null: false
      t.integer :weight, null: false
      t.integer :available, null: false
      t.belongs_to :product_category, foreign_key: true, null: false
      t.timestamps
    end
  end
end
