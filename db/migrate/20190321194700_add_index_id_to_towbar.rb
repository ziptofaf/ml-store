class AddIndexIdToTowbar < ActiveRecord::Migration[5.2]
  def change
    add_column :towbars, :index_id, :integer, null: false
  end
end
