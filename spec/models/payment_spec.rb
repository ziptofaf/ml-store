require 'rails_helper'

RSpec.describe Payment, type: :model do
  it 'correctly fills paid_at field automatically when needed' do
    payment = FactoryBot.create(:payment, payment_system: 'paypal')
    expect(payment.paid_at).to eq nil
    payment = FactoryBot.create(:payment, payment_system: 'cash_on_delivery')
    expect(payment.paid_at > 10.seconds.ago).to eq true
  end
end
