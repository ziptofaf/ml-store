require 'rails_helper'

RSpec.describe Towbar, type: :model do
  it 'has a correct description' do
    plug = FactoryBot.create(:plug)
    towbar = FactoryBot.create(:towbar, plug: plug)
    expect(towbar.description_with_plug).to eq(towbar.description + "\n" + plug.description)
  end

  it 'correctly identifies available scope' do
    FactoryBot.create(:towbar, name: 'S7', available: 1)
    FactoryBot.create(:towbar, name: 'S7TT', available: 0)
    FactoryBot.create(:towbar, name: 'S13TT', available: -10)
    expect(Towbar.available.count).to eq 1
  end

  it 'is possible to import a towbar' do
    FactoryBot.create(:product_category, id: 1)
    path = Rails.root.join('spec', 'sample_files', 'towbar.json')
    Towbar.import_from_json(path)
    expect(Towbar.count).to eq 1
  end

  it 'throws an exception if towbar file has any errors' do
    FactoryBot.create(:product_category, id: 1)
    path = Rails.root.join('spec', 'sample_files', 'bugged_towbar.json')
    expect { Towbar.import_from_json(path) }.to raise_error(ActiveRecord::NotNullViolation)
  end

end
