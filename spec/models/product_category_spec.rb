require 'rails_helper'

RSpec.describe ProductCategory, type: :model do
  it 'is possible to create product categories and generated tree is correct' do
    root = FactoryBot.create(:product_category)
    FactoryBot.create(:product_category, name: 'Towbar1', root_category: root.id)
    FactoryBot.create(:product_category, name: 'Towbar2', root_category: root.id)
    child = FactoryBot.create(:product_category, name: 'Towbar3', root_category: root.id)
    FactoryBot.create(:product_category, name: 'Child1', root_category: child.id)
    expect root.as_tree.children.count == 3
    expect root.as_tree.children.first.children.count == 0
    expect root.as_tree.children.last.children.count == 1
  end

  it 'is possible to import categories from DOPS and keep them updated' do
    path = Rails.root.join('spec', 'sample_files', 'product_categories.json')
    ProductCategory.import_from_json(path)
    expect(ProductCategory.count).to eq 2
    # on purpose, we make sure it won't create new records
    ProductCategory.import_from_json(path)
    expect(ProductCategory.count).to eq 2
  end

  it 'detects roots correctly' do
    root = FactoryBot.create(:product_category, name: 'Towbar')
    FactoryBot.create(:product_category, name: 'Child1', root_category: root.id)
    expect(ProductCategory.roots).to eq [root]
  end

end
