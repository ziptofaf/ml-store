require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  it "is correctly created and polymorphic association is generated as well" do
    item = FactoryBot.create(:order_item)
    expect(item.sellable.id.nil?).to eq false
  end
  it 'has a correct hash value when calling hash_with_sellable' do
    item = FactoryBot.create(:order_item)
    hash = item.hash_with_sellable
    expect(hash['sellable'][:price].to_f).to eq 9.95
  end
end
