require 'rails_helper'

RSpec.describe Plug, type: :model do
  it 'correctly identifies old_plugs scope' do
    FactoryBot.create(:plug, name: 'S7')
    FactoryBot.create(:plug, name: 'S7TT')
    FactoryBot.create(:plug, name: 'S13')
    FactoryBot.create(:plug, name: 'S13TT')
    FactoryBot.create(:plug, name: 'OS13T1')
    FactoryBot.create(:plug, name: 'N7')
    expect(Plug.old_plugs.count).to eq 4
  end

  it 'correctly identifies available scope' do
    FactoryBot.create(:plug, name: 'S7', available: 1)
    FactoryBot.create(:plug, name: 'S7TT', available: 0)
    FactoryBot.create(:plug, name: 'S13', available: 99)
    FactoryBot.create(:plug, name: 'S13TT', available: -10)
    FactoryBot.create(:plug, name: 'OS13T1', available: 42)
    FactoryBot.create(:plug, name: 'N7', available: 100)
    expect(Plug.available.count).to eq 4
  end


  it 'is possible to import a plug' do
    FactoryBot.create(:product_category, id: 1)
    path = Rails.root.join('spec', 'sample_files', 'plug.json')
    Plug.import_from_json(path)
    expect(Plug.count).to eq 1
  end

end
