# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order, type: :model do
  it 'uses fill_delivery_from_billing correctly' do
    order = FactoryBot.build(:order, billing_name: 'Adolf', billing_surname: 'Sample', billing_city: 'Bangalore')
    order.fill_delivery_from_billing
    order.save # fill_delivery_from_billing is not guaranteed to work before saving a record (due to attr_encrypted)
    order = Order.first
    expect(order.delivery_name).to eq 'Adolf'
    expect(order.delivery_surname).to eq 'Sample'
    expect(order.delivery_city).to eq 'Bangalore'
  end

  it 'correctly calculates combined price of all items' do
    order = FactoryBot.create(:order)
    FactoryBot.create(:order_item, order: order)
    FactoryBot.create(:order_item, order: order)
    expect(order.price).to eq 19.9
  end

  it 'allows for encryption_key_deletion and displays [deleted] in fields treated as such' do
    order = FactoryBot.build(:order, billing_name: 'Adolf', billing_surname: 'Sample', billing_city: 'Bangalore')
    order.fill_delivery_from_billing
    order.save # fill_delivery_from_billing is not guaranteed to work before saving a record (due to attr_encrypted)
    order.delete_encryption_key
    order = Order.first
    expect(order.delivery_name).to eq '[deleted]'
    expect(order.delivery_surname).to eq '[deleted]'
    expect(order.delivery_city).to eq '[deleted]'
  end

  it 'always returns an empty array for outdated records as orders currently do not expire' do
    FactoryBot.create(:order, created_at: 10.years.ago)
    expect(Order.has_personal_information?).to eq true
    expect(Order.outdated_records).to eq []
    expect(Order.count).to eq 1
    expect(Order.can_expire?).to eq false
  end
end
