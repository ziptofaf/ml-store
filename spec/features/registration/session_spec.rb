# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User sessions', type: 'request' do
  it 'is possible to register' do
    hash = {
      user: {
        email: 'sampleuser@sample.com',
        password: 'password123',
        password_confirmation: 'password123'
      }
    }
    post '/users.json', params: hash
    expect(response).to have_http_status :created
  end

  it 'is possible to obtain a token with correct credentials' do
    FactoryBot.create(:user)
    login_hash = {
      grant_type: 'password',
      email: 'sample@example.com',
      password: 'samplepassword'
    }
    post '/oauth/token', params: login_hash
    expect(response).to have_http_status 200
  end
  it 'is not possible to obtain a token without correct credentials' do
    FactoryBot.create(:user)
    login_hash = {
      grant_type: 'password',
      email: 'sample@example.com',
      password: 'samplepassword123'
    }
    post '/oauth/token', params: login_hash
    expect(response).to have_http_status 400
  end

  it 'is possible to change your credentials if you have a token and a correct current password' do
    login_header = LoginHelper.obtain_valid_header
    hash = {
      user: {
        current_password: 'samplepassword',
        password: 'password456',
        password_confirmation: 'password456'
      }
    }
    put '/users.json', params: hash, headers: login_header
    expect(response).to have_http_status 204
    u = User.first
    expect(u.valid_password?('password456')).to eq true
  end

  it 'is not possible to change your password if you have a token but dont know current password' do
    login_header = LoginHelper.obtain_valid_header
    hash = {
      user: {
        current_password: 'samplepassword134',
        password: 'password456',
        password_confirmation: 'password456'
      }
    }
    put '/users.json', params: hash, headers: login_header
    expect(response.body.include?('is invalid')).to eq true
    expect(response).to have_http_status 422
    u = User.first
    expect(u.valid_password?('password456')).to eq false
  end

  it 'is NOT  possible to change your credentials if you dont have a token' do
    LoginHelper.obtain_valid_header # we need it to create a user
    hash = {
      user: {
        current_password: 'samplepassword',
        password: 'password456',
        password_confirmation: 'password456'
      }
    }
    put '/users.json', params: hash
    expect(response).to have_http_status 401
  end
end
