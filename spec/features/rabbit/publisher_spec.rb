# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Rabbit::Publisher' do
  it 'is only possible to send a json string to queue' do
    test_data = { 'abc' => 'def' }.to_json
    expect(Rabbit::Publisher.publish(test_data, 'test_queue', false)).to eq true
    test_data = 'abcdef'
    expect(Rabbit::Publisher.publish(test_data, 'test_queue', false)).to eq false
  end
end