# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Rabbit::OrdersPublisher' do
  it 'is only possible to send a json string to queue' do
    order = FactoryBot.create(:order)
    FactoryBot.create(:payment, order: order, payment_system: 'cash_on_delivery')
    FactoryBot.create(:order_item, order: order)
    expect(Rabbit::OrdersPublisher.send_new_orders_to_queue).to eq true
  end
end