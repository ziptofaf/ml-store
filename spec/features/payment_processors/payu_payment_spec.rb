# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Payu Payment', type: 'request' do
  let(:files) do
    {
        register_file: File.open(Rails.root.join('spec', 'sample_files', 'payu', 'token.json')).read,
        response_creation_file: File.open(Rails.root.join('spec', 'sample_files', 'payu', 'payment_creation_response.json')).read
    }
  end
  it 'is possible to go through the whole process of generating payment url' do
    require 'fakeweb'
    order = FactoryBot.create(:order)
    FactoryBot.create(:order_item, order: order)
    FakeWeb.register_uri(:post, 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize',
                         content_type: 'application/json', body: files[:register_file])
    FakeWeb.register_uri(:post, 'https://secure.snd.payu.com/api/v2_1/orders',
                         content_type: 'application/json', body: files[:response_creation_file], status: ['302', 'Found'])
    payment = FactoryBot.create(:payment, payment_system: 'payu', order: order)
    expect(payment.url).to eq 'https://payu.com/testowa-platnosc'
  end

  it 'raises an exception if for any reason you cant obtain registration token' do
    require 'fakeweb'
    order = FactoryBot.create(:order)
    FactoryBot.create(:order_item, order: order)
    FakeWeb.register_uri(:post, 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize',
                         content_type: 'application/json', body: files[:register_file], status: ['401', 'Unauthorized'])
    expect {
      FactoryBot.create(:payment, payment_system: 'payu', order: order)
    }.to raise_error(PaymentProcessors::Abstract::TokenObtainingFailure)
  end
end
