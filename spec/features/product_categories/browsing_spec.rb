# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Product categories listings', type: 'request' do
  it 'it correctly lists root categories if no params are set' do
    FactoryBot.create(:product_category, name: 'Towbars')
    get '/product_categories.json'
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result.length).to eq 1
    expect(result[0]['name']).to eq 'Towbars'
  end
  it 'it returns a 400 and error message if you query for a category root that doesnt exist' do
    FactoryBot.create(:product_category, name: 'Towbars')
    get '/product_categories.json?root_category=999'
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['status']).to eq 'error'
  end
  it 'it correctly returns children if root category is set' do
    root = FactoryBot.create(:product_category, name: 'Towbars')
    FactoryBot.create(:product_category, name: 'ChildTowbar', root_category: root.id)
    get "/product_categories.json?root_category=#{root.id}"
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result.length).to eq 1
    expect(result[0]['name']).to eq 'ChildTowbar'
  end


end
