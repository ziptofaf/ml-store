# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Products listings', type: 'request' do
  before(:each) do
    @category = FactoryBot.create(:product_category, name: 'SampleCategory')
    @towbar = FactoryBot.create(:towbar, product_category: @category, name: 'MrTowbar')
    @plug = FactoryBot.create(:plug, product_category: @category, name: 'MrPlug')
  end

  it 'is possible to see plugs and towbars for a given category' do
    get "/products.json?product_type=towbar&product_category=#{@category.id}"
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result.length).to eq 1
    expect(result[0]['name']).to eq 'MrTowbar'
    get "/products.json?product_type=plug&product_category=#{@category.id}"
    result = JSON.parse(response.body)
    expect(result.length).to eq 1
    expect(result[0]['name']).to eq 'MrPlug'
  end

  it 'returns an empty array if you use a valid product_category with no products' do
    category = FactoryBot.create(:product_category, name: 'SampleCategory2')
    get "/products.json?product_type=towbar&product_category=#{category.id}"
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result.length).to eq 0
  end

  it 'returns 400 error for any combination of invalid product_type|product_category' do
    get "/products.json?product_type=towbar&product_category=#{@category.id + 10}"
    expect(response).to have_http_status 400
    get "/products.json?product_type=cowbar&product_category=#{@category.id}"
    expect(response).to have_http_status 400
    get "/products.json?product_type=plug&product_category=#{@category.id + 10}"
    expect(response).to have_http_status 400
  end

  it 'returns product details for a towbar or plug' do
    get "/products/#{@towbar.id}.json?product_type=towbar"
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result['name']).to eq 'MrTowbar'

    get "/products/#{@plug.id}.json?product_type=plug"
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result['name']).to eq 'MrPlug'
  end

  it 'does not allow entering invalid id or product_type' do
    get "/products/#{@towbar.id + 1}.json?product_type=towbar"
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['message']).to eq 'Product id is invalid'

    get "/products/#{@plug.id + 1}.json?product_type=plug"
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['message']).to eq 'Product id is invalid'

    get "/products/#{@towbar.id + 1}.json?product_type=cowbars"
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['message']).to eq 'Product Type is invalid'
  end




end
