# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Orders', type: :request do
  before(:each) do
    @category = FactoryBot.create(:product_category, name: 'SampleCategory')
    @towbar = FactoryBot.create(:towbar, product_category: @category, name: 'MrTowbar')
    @plug = FactoryBot.create(:plug, product_category: @category, name: 'MrPlug')

    @req_body = { email: 'sample@sample.com', billing_name: 'Billy', billing_surname: 'Billy',
                  billing_phone: '123-456-789', billing_address: 'Sample Street', billing_post_code: '00-123', delivery_city: 'Neverwhere',
                  billing_country: 'Poland', delivery_name: 'Billy', delivery_surname: 'Billy', billing_city: 'Neverwhere',
                  delivery_phone: '123-456-789', delivery_address: 'Sample Street', delivery_post_code: '00-123',
                  delivery_country: 'Poland' }
    @req_item_position = { order_items_attributes: [{ quantity: 2, sellable_type: 'Towbar', sellable_id: @towbar.id }] }
    @req_payment = { payment_attributes: { payment_system: 'wired' } }
  end

  it 'a correctly submitted order will be saved in the database' do
    post '/orders.json', params: { order: @req_body.merge(@req_item_position).merge(@req_payment) }
    expect(response).to have_http_status :ok
    result = JSON.parse(response.body)
    expect(result['price']).to eq (@towbar.price * 2).to_s
    expect(result['state']).to eq 'new'
    expect(Order.count).to eq 1
    expect(Order.recently_updated.count).to eq 1
  end

  it 'will mark order as ready to ship if you choose cash_on_delivery' do
    @req_payment[:payment_attributes][:payment_system] = 'cash_on_delivery'
    post '/orders.json', params: { order: @req_body.merge(@req_item_position).merge(@req_payment) }
    expect(response).to have_http_status :ok
    expect(Order.count).to eq 1
    expect(Payment.first.paid_at.nil?).to eq false
    expect(Order.recently_updated.count).to eq 1
  end

  it 'complains about no item positions if you dont define any' do
    post '/orders.json', params: { order: @req_body.merge(@req_payment) }
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['status']).to eq 'error'
    expect(result['message']).to eq 'Order items are incorrect'
  end

  it 'complains if you use a nonexistent item_position' do
    req_item_position = { order_items_attributes: [{ quantity: 2, sellable_type: 'Towbar', sellable_id: 0 }] }
    post '/orders.json', params: { order: @req_body.merge(@req_payment).merge(req_item_position) }
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['status']).to eq 'error'
    expect(result['message']).to eq 'Could not save an order'
  end

  it 'complains if you forget to add payment info' do
    post '/orders.json', params: { order: @req_body.merge(@req_item_position) }
    expect(response).to have_http_status 400
    result = JSON.parse(response.body)
    expect(result['status']).to eq 'error'
    expect(result['message']).to eq 'Payment system is undefined'
  end

end