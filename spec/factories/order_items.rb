FactoryBot.define do
  factory :order_item do
    association :sellable, factory: :towbar
    quantity { 1 }
    order
  end
end
