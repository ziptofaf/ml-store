FactoryBot.define do
  factory :payment do
    order
    payment_system { 'wired' }
  end
end
