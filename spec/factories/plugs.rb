FactoryBot.define do
  factory :plug do
    name { 'MyString' }
    socket { 1 }
    description { 'plug description' }
    dedicated { false }
    indicator_failure_bulb { false }
    parking_sensors_switch { false }
    permanent_power { false }
    bypass_relay { false }
    activation { false }
    price { '9.99' }
    weight { 1 }
    available { 1 }
    product_category
    index_id { 2 }
  end
end
