FactoryBot.define do
  factory :user do
    email { 'sample@example.com' }
    password { 'samplepassword' }
    password_confirmation { 'samplepassword' }

    initialize_with { User.find_or_create_by email: email }
  end

end
