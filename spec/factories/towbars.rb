FactoryBot.define do
  factory :towbar do
    name { 'MyString' }
    description { 'towbar description' }
    plug { nil }
    ball_type { :fixed }
    price { '9.95' }
    weight { 1 }
    available { 1 }
    product_category
    index_id { 1000 }
  end
end
