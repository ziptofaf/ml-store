FactoryBot.define do
  factory :encryption_credential do
    source_id { "MyString" }
    encryption_key { "MyText" }
  end
end
