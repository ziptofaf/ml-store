FactoryBot.define do
  factory :product_category do
    root_category { nil }
    name { 'Towbar' }
  end
end
