# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    email { 'samplemail@example.com' }
    billing_name { 'Mr Sample' }
    billing_surname { 'Kowalski' }
    billing_phone { '123 456 789' }
    billing_address { 'Piekiełko 11' }
    billing_city { 'Slupsk' }
    billing_post_code { '76-200' }
    billing_country { 'pl' }
    delivery_name { 'Mr Sample' }
    delivery_surname { 'Kowalski' }
    delivery_phone { '123 456 789' }
    delivery_address { 'Piekiełko 11' }
    delivery_city { 'Slupsk' }
    delivery_post_code { '76-200' }
    delivery_country { 'pl' }
    state { 'new' }
    comment { 'clients comment' }
    package_tracking_url { nil }
  end
end
