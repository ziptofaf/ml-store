FactoryBot.define do
  factory :payment_authentication do
    token { "MyString" }
    payment_system { "payu" }
  end
end
