# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PaymentListeners::PayuController, type: :controller do
  let(:files) do
    {
        register_file: File.open(Rails.root.join('spec', 'sample_files', 'payu', 'token.json')).read,
        response_creation_file: File.open(Rails.root.join('spec', 'sample_files', 'payu', 'payment_creation_response.json')).read
    }
    end
  before(:each) do
    @order = FactoryBot.create(:order)
    FactoryBot.create(:order_item, order: @order)
    FakeWeb.register_uri(:post, 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize',
                         content_type: 'application/json', body: files[:register_file])
    FakeWeb.register_uri(:post, 'https://secure.snd.payu.com/api/v2_1/orders',
                         content_type: 'application/json', body: files[:response_creation_file], status: ['302', 'Found'])
    @payment = FactoryBot.create(:payment, payment_system: 'payu', order: @order)
    @hash = {
      'order' => { 'orderId' => 'ABCDEF', 'extOrderId' => @order.id,
                   'merchantPosId' => '356187', 'status' => 'COMPLETED' }
    }
  end

  describe 'POST #listen' do
    it 'updates a payment correctly if hash is valid' do
      post :listen, as: :json, params: @hash
      expect(response).to have_http_status(:success)
      expect(@payment.reload.paid_at.nil?).to eq false
    end

    it 'does not update payment if payment has not been completed yet' do
      @hash['order']['status'] = 'NEW'
      post :listen, as: :json, params: @hash
      expect(response).to have_http_status(:success)
      expect(@payment.reload.paid_at.nil?).to eq true
    end

    it 'returns an ok status even if order does not exist' do
      @hash['order']['extOrderId'] = 12345
      post :listen, as: :json, params: @hash
      expect(response).to have_http_status(:success)
      expect(@payment.reload.paid_at.nil?).to eq true
    end
    it 'does not let payu update a payment that should be processed by something else' do
      @payment.destroy
      @payment = FactoryBot.create(:payment, payment_system: 'paypal', order: @order)
      post :listen, as: :json, params: @hash
      expect(response).to have_http_status(:success)
      expect(@payment.reload.paid_at.nil?).to eq true
    end
  end
end
