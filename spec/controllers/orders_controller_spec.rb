# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrdersController, type: :controller do

  before(:each) do
    login_header = LoginHelper.obtain_valid_header
    request.headers.merge! login_header
    order = FactoryBot.create(:order, user: User.first)
    @id = order.id
    FactoryBot.create(:order_item, order: order)
    FactoryBot.create(:payment, order: order)
  end

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
      body = JSON.parse(response.body)
      expect(body.count).to eq 1
      expect(body[0]['price']).to eq '9.95'
    end

    it 'returns 401 http failure if you use invalid credentials' do
      login_header = LoginHelper.obtain_valid_header
      login_header[:Authorization] = ''
      request.headers.merge! login_header
      get :index
      expect(response).to have_http_status(401)
    end
  end

  describe 'GET #show' do
    it 'returns http failure without uuid' do
      get :show, params: { id: 1234 }
      expect(response).to have_http_status(404)
    end
  end

  describe 'GET #show' do
    it 'returns http success and order details with correct uuid' do
      get :show, params: { id: @id }
      expect(response).to have_http_status(:success)
      body = JSON.parse(response.body)
      expect(body['price']).to eq '9.95'
      expect(body['id']).to eq @id
    end
  end


end
