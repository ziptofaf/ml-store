require 'rails_helper'

RSpec.describe OrderItemsController, type: :controller do

  before(:each) do
    @position = FactoryBot.create(:order_item)

  end

  describe "GET #index" do
    it "returns http success if guid is set" do
      get :index, params: { order_id: @position.order.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #index" do
    it "returns http not found if guid is not set or invalid" do
      get :index
      expect(response).to have_http_status(404)
      get :index, params: { order_id: 'abcdef' }
      expect(response).to have_http_status(404)
    end
  end

end
