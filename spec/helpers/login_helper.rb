module LoginHelper
  def self.obtain_valid_header
    require 'rest-client'
    user = FactoryBot.create(:user)
    token = Doorkeeper::AccessToken.create!(application_id: nil, resource_owner_id: user.id).token
    { Authorization: "Bearer #{token}" }
  end

end