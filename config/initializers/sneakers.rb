# frozen_string_literal: true

if Rails.env.development?
  Sneakers.configure(timeout_job_after: 20.seconds, workers: 1, threads: 2, prefetch: 2, pid_path: './tmp/pids/sneakers.pid')
elsif Rails.env.staging?
  Sneakers.configure(timeout_job_after: 20.seconds, workers: 1, threads: 2, prefetch: 2, daemonize: true, pid_path: './tmp/pids/sneakers.pid')
else
  connection = Bunny.new(host: 'dops.fessio.xyz', user: 'shop', password: Rails.application.credentials.shop_queue_password)
  Sneakers.configure(connection: connection, timeout_job_after: 20.seconds, workers: 1, threads: 2, prefetch: 2, daemonize: true, pid_path: './tmp/pids/sneakers.pid')
end
Sneakers.logger.level = Logger::INFO # the default DEBUG is too noisy
