set :application, 'ml-store'
set :repo_url, 'git@gitlab.com:ziptofaf/ml-store.git'
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
append :linked_files, "config/database.yml", "config/master.key", "config/credentials_database.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/images"

desc 'kill current sneakers instance'
task :kill_sneakers do
  on roles(:app) do |host|
    within release_path do
      command = "exec ruby sneakers_manager.rb -action=kill"
      execute :bundle, command.to_s
    end
  end
end

desc 'start a new sneakers instance'
task :start_sneakers do
  on roles(:app) do |host|
    within release_path do
      with rails_env: fetch(:rails_env) do
        command = "exec ruby -e \"require \'sneakers/spawner\';Sneakers::Spawner.spawn\""
        execute :bundle, command.to_s
      end
    end
  end
end

task :custom_stop_puma do
  on roles(:app) do |host|
    within release_path do
      execute "kill $(cat #{shared_path}/tmp/pids/puma.pid)"
    rescue
      p "Process is already stopped"
    end
  end
end

after 'deploy:published', :custom_stop_puma
after 'deploy:published', 'restart' do
  invoke 'delayed_job:restart'
end
after 'deploy:published', :kill_sneakers
after 'deploy:finished', :start_sneakers

set :pty, true
set :use_sudo, false
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_workers, 2
set :puma_threads, [1, 6]
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"