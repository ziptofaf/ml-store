Rails.application.routes.draw do


  get 'order_items/index'
  resources :orders, only: [:index, :show, :create]
  resources :product_categories, only: [:index]
  resources :products, only: [:index, :show]
  use_doorkeeper
  devise_for :users, skip: [:sessions, :password], controllers: {
    registrations: 'users/registrations'
  }
  namespace :payment_listeners do
    post 'payu', to: 'payu#listen'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
